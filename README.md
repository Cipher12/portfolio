## [Portfolio](https://chiper.dev)

Static website generated using [Hugo](https://gohugo.io/). Using the [hermit](https://github.com/Track3/hermit) theme.

Hosted on [Firebase](https://firebase.google.com/).

Using a GitLab pipeline to automatically deploy on Firebase each commit that's pushed to master.

